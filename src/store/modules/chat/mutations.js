const { _types: { mutations } } = require('./types');

export default {
  [mutations.SET_MESSAGES]: (state, messages) => {
    state.messages = messages;
  },
};
