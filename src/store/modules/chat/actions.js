import Api from '@/api';

const { _types: { actions, mutations } } = require('./types');

const getMessages = ({ commit }) => {
  Api.fetchMessages
    .then((response) => {
      commit(mutations.SET_MESSAGES, response);
    })
    .catch((error) => {
      // eslint-disable-next-line
      console.error(error);
    });
};

export default {
  [actions.GET_MESSAGES]: getMessages,
};
