const { _types: { getters } } = require('./types');

export default {
  [getters.GET_MESSAGES]: state => state.messages,
};
