import removeNamespace from '@/util/namespace-helper';

// Public types
export const Types = {
  getters: {
    GET_MESSAGES: 'chat/getMessages',
  },
  mutations: {
    SET_MESSAGES: 'chat/SET_MESSAGES',
  },
  actions: {
    GET_MESSAGES: 'chat/getMessages',
    // FETCH_USER_INFO: 'chat/getUserInfo',
  },
};

// Private types
export const _types = removeNamespace('chat/', Types);
