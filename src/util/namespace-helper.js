/* eslint-disable no-param-reassign */
import reduce from 'lodash-es/reduce';

export default function removeNamespace(namespace, types) {
  return reduce(
    types,
    (typeObj, typeValue, typeName) => {
      typeObj[typeName] = reduce(
        typeValue,
        (obj, v, k) => {
          obj[k] = v.replace(namespace, '');
          return obj;
        },
        {},
      );
      return typeObj;
    },
    {},
  );
}
